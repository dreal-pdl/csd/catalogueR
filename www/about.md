#### Objectif

Le Catalogue de données de la DREAL vise à faciliter la découverte et l'accès aux données stockées dans les bases de donnés internes de la DREAL.

#### Fonctionnalités

L'onglet **"Rechercher"** permet :
- d'effectuer une recherche textuelle à partir de mots-clés au d'expression,
- d'afficher en résultat un tableau de [tables](https://fr.wikipedia.org/wiki/Table_(base_de_donn%C3%A9es)) correspondant à la recherche.

L'onglet **"Explorer"** :
- de naviguer parmi les bases de données, schémas et tables associés,
- d'afficher en résultat un tableau de schémas ou de tables correspondants à la recherche.

Les deux options permettent ensuite de visualiser :
- le [dictionnaire des données](https://fr.wikipedia.org/wiki/Dictionnaire_des_donn%C3%A9es),
- le tableau des données,
- un aperçu des données spatiales.

#### Charte de nommage

Les noms de tous les objets des bases de données (schémas, tables, champs, etc.) doivent être choisis selon une charte présentée dans la fiche de procédure [Organisation des bases de données sur le SGBD PostgreSQL](http://set-pdl-wiki.dreal-pdl.ad.e2.rie.gouv.fr/outils/sgbd/organisation-bases-postgresql).
