library(logging)

Sys.setenv("HTTP_PROXY" = "http://pfrie-std.proxy.e2.rie.gouv.fr:8080")
Sys.setenv("HTTPS_PROXY" = "http://pfrie-std.proxy.e2.rie.gouv.fr:8080")
# a n'utiliser que lorsque le fichier est exécuté par la tache planifiée
args <- commandArgs(trailingOnly = FALSE)
script_path <- sub("--file=", "", args[grep("--file=", args)])
project_path <- normalizePath(file.path(dirname(script_path), ".."))
setwd(project_path)

# Configuration du logging ------------------------------------------------------------------------------------------------------------
basicConfig(level = 'DEBUG')

log_file <- "logs/script_routine.log"
log_con <- file(log_file, open = "wt")


# Fonction pour exécuter un script et capturer les logs -------------------------------------------------------------------------------
execute_script <- function(script_path) {
  tryCatch({
    source(script_path)
    loginfo("Script exécuté avec succès : %s", script_path)
    return(TRUE)
  }, error = function(e) {
    logerror("Erreur lors de l'exécution du script %s : %s", script_path, e$message)
    return(FALSE)
  })
}

sink(log_con, type = "message")
sink(log_con, type = "output")

# Exécution de la mise à jour du datamart ---------------------------------------------------------------------------------------------
start_time <- Sys.time()
datamart_success <- execute_script("data-raw/datamartage.R")
end_time <- Sys.time()

if (datamart_success) {
  execution_time <- end_time - start_time
  loginfo("Mise à jour du datamart catalogueR réussie")

  # Récupérer la taille du datamart
  datamart_file <- "datamart_catalogue.RData"
  datamart_size <- file.info(datamart_file)$size
  loginfo("Taille du datamart : %s bytes", datamart_size)
  loginfo("Temps d'exécution : %s minutes", execution_time)

} else {
  logerror("Échec de la mise à jour du datamart catalogueR")
}

# Exécution du déploiement de l'application -------------------------------------------------------------------------------------------
deploy_success <- execute_script("dev/deploiement_automatique.R")
if (deploy_success) {
  loginfo("Déploiement automatique du datamart réussi")
} else {
  logerror("Échec du déploiement automatique du datamart")
}

# Fermer les connexions de fichier ----------------------------------------------------------------------------------------------------
sink(type = "message")
sink(type = "output")
close(log_con)

